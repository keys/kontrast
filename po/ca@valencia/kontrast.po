# Translation of kontrast.po to Catalan (Valencian)
# Copyright (C) 2020-2022 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2020.
# Josep M. Ferrer <txemaq@gmail.com>, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kontrast\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-06 00:39+0000\n"
"PO-Revision-Date: 2022-09-14 16:18+0200\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca@valencia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.12.0\n"

#: contents/ui/FavoritePage.qml:16
#, kde-format
msgid "Favorite colors"
msgstr "Colors preferits"

#: contents/ui/FavoritePage.qml:50
#, kde-format
msgid "Text: %1"
msgstr "Text: %1"

#: contents/ui/FavoritePage.qml:70
#, kde-format
msgid "Background: %1"
msgstr "Fons: %1"

#: contents/ui/FavoritePage.qml:91
#, kde-format
msgid "Remove"
msgstr "Elimina"

#: contents/ui/FavoritePage.qml:103
#, kde-format
msgid "Background color copied to clipboard"
msgstr "Color de fons copiat al porta-retalls"

#: contents/ui/FavoritePage.qml:111
#, kde-format
msgid "Text color copied to clipboard"
msgstr "Color del text copiat al porta-retalls"

#: contents/ui/FavoritePage.qml:120
#, kde-format
msgid "Color copied to clipboard"
msgstr "Color copiat al porta-retalls"

#: contents/ui/HelpPage.qml:13 contents/ui/main.qml:40
#, kde-format
msgid "Help"
msgstr "Ajuda"

#: contents/ui/HelpPage.qml:19
#, kde-format
msgid "Contrast"
msgstr "Contrast"

#: contents/ui/HelpPage.qml:24
#, kde-format
msgid ""
"Contrast is the difference in color between two objects that allows them to "
"be distinguished. In visual perception of the real world, contrast is "
"determined by the difference in the color and brightness of the object and "
"other objects within the same field of view."
msgstr ""
"El contrast és la diferència al color entre dos objectes que els permet "
"distingir. En la percepció visual del món real, el contrast està determinat "
"per la diferència en el color i la brillantor de l'objecte i altres objectes "
"dins del mateix camp de visió."

#: contents/ui/HelpPage.qml:29
#, kde-format
msgid ""
"A contrast value of 21 indicates a perfect contrast (usually black on "
"white), and a value of 0 indicate that the two colors are the same."
msgstr ""
"Un valor de contrast de 21 indica un contrast perfecte (normalment negre "
"sobre blanc), i un valor de 0 indica que els dos colors són el mateix."

#: contents/ui/HelpPage.qml:34
#, kde-format
msgid ""
"For normal text, the contrast ratio should be at least of 4.5 to conform to "
"the WCAG AA standard and a contrast ratio of 7 or more is required to "
"conform with the WCAG AAA standard."
msgstr ""
"Per al text normal, la relació del contrast ha de ser com a mínim de 4,5 de "
"conformitat amb l'estàndard WCAG AA, i es requerix una relació de contrast "
"de 7 o més de conformitat amb l'estàndard WCAG AAA."

#: contents/ui/HelpPage.qml:39
#, kde-format
msgid ""
"For large text, the contrast ratio should be at least of 3 to conform to the "
"WCAG AA standard and a contrast ratio of 4.5 or more is required to conform "
"with the WCAG AAA standard."
msgstr ""
"Per al text gran, la relació del contrast ha de ser com a mínim de 3 de "
"conformitat amb l'estàndard WCAG AA, i es requerix una relació de contrast "
"de 4,5 o més de conformitat amb l'estàndard WCAG AAA."

#: contents/ui/main.qml:23
#, kde-format
msgid "kontrast"
msgstr "kontrast"

#: contents/ui/main.qml:28 contents/ui/MainPage.qml:20
#, kde-format
msgid "Contrast Checker"
msgstr "Comprovador del contrast"

#: contents/ui/main.qml:34
#, kde-format
msgid "Favorite Colors"
msgstr "Colors preferits"

#: contents/ui/main.qml:46
#, kde-format
msgid "About"
msgstr "Quant a"

#: contents/ui/MainPage.qml:42
#, kde-format
msgid "Contrast ratio: %1"
msgstr "Relació de contrast: %1"

#: contents/ui/MainPage.qml:50
#, kde-format
msgid "%1"
msgstr "%1"

#: contents/ui/MainPage.qml:62
#, kde-format
msgid "Perfect for normal and large text"
msgstr "Perfecte per a text normal i gran"

#: contents/ui/MainPage.qml:62
#, kde-format
msgid "Perfect for large text and good for normal text"
msgstr "Perfecte per a text gran i bo per a text normal"

#: contents/ui/MainPage.qml:62
#, kde-format
msgid "Good for large text and bad for normal text"
msgstr "Bé per a text gran i roín per a text normal"

#: contents/ui/MainPage.qml:62
#, kde-format
msgid "Bad for large and normal text"
msgstr "Roín per a text gran i bé per a text normal"

#: contents/ui/MainPage.qml:69
#, kde-format
msgid ""
"Contrast is the difference in luminance or color that makes an object (or "
"its representation in an image or display) distinguishable. In visual "
"perception of the real world, contrast is determined by the difference in "
"the color and brightness of the object and other objects within the same "
"field of view."
msgstr ""
"El contrast és la diferència en la luminància o color que fa que un objecte "
"(o la seua representació en una imatge o pantalla) siga distingible. En la "
"percepció visual del món real, el contrast està determinat per la diferència "
"en el color i la brillantor de l'objecte i altres objectes dins del mateix "
"camp de visió."

#: contents/ui/MainPage.qml:84
#, kde-format
msgid "Text"
msgstr "Text"

#: contents/ui/MainPage.qml:113 contents/ui/MainPage.qml:193
#, kde-format
msgid "Hue %1°"
msgstr "To %1°"

#: contents/ui/MainPage.qml:127 contents/ui/MainPage.qml:207
#, kde-format
msgid "Saturation %1"
msgstr "Saturació %1"

#: contents/ui/MainPage.qml:141 contents/ui/MainPage.qml:221
#, kde-format
msgid "Lightness %1"
msgstr "Lluminositat %1"

#: contents/ui/MainPage.qml:165
#, kde-format
msgid "Background"
msgstr "Fons"

#: contents/ui/MainPage.qml:238
#, kde-format
msgid "Font Size %1px"
msgstr "Mida del tipus de lletra %1 px"

#: contents/ui/MainPage.qml:255
#, kde-format
msgid "Invert"
msgstr "Invertix"

#: contents/ui/MainPage.qml:261
#, kde-format
msgid "Randomize"
msgstr "Aleatori"

#: contents/ui/MainPage.qml:267
#, kde-format
msgid "Mark as favorite"
msgstr "Marca com a preferit"

#: contents/ui/MainPage.qml:270
#, kde-format
msgid "Failed to save color"
msgstr "No s'ha pogut guardar el color"

#: kontrast.cpp:349
#, kde-format
msgid "Font size %1px is bad with the current contrast"
msgstr "La mida del tipus de lletra %1 px és roïna amb el contrast actual"

#: kontrast.cpp:352
#, kde-format
msgid "Font size %1px is good with the current contrast"
msgstr "La mida del tipus de lletra %1 px és bona amb el contrast actual"

#: kontrast.cpp:355
#, kde-format
msgid "Font size %1px is perfect with the current contrast"
msgstr "La mida del tipus de lletra %1 px és perfecta amb el contrast actual"

#: main.cpp:43
#, kde-format
msgctxt "@title"
msgid "Kontrast"
msgstr "Kontrast"

#: main.cpp:45
#, kde-format
msgctxt "@title"
msgid "A contrast checker application"
msgstr "Una aplicació de comprovació del contrast"

#: main.cpp:48
#, kde-format
msgctxt "@info:credit"
msgid "Carl Schwan"
msgstr "Carl Schwan"

#: main.cpp:49
#, kde-format
msgctxt "@info:credit"
msgid "Maintainer and creator"
msgstr "Mantenidor i creador"

#: main.cpp:52
#, kde-format
msgctxt "@info:credit"
msgid "Wikipedia"
msgstr "Viquipèdia"

#: main.cpp:52
#, kde-format
msgctxt "@info:credit"
msgid "Text on the main page CC-BY-SA-4.0"
msgstr "Text en la pàgina principal CC-BY-SA-4.0"

#: main.cpp:53
#, kde-format
msgctxt "@info:credit"
msgid "Carson Black"
msgstr "Carson Black"

#: main.cpp:53
#, kde-format
msgctxt "@info:credit"
msgid "SQLite backend for favorite colors"
msgstr "Dorsal SQLite per als colors preferits"

#: main.cpp:54
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Antoni Bella"

#: main.cpp:54
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "antonibella5@yahoo.com"
